default: build test

MPICXX = mpicxx

MPIEXEC = mpirun
NPFLAG = -n 2

PYTHON = python3
PYTHON_CONFIG = ${PYTHON}-config

MPI4PY_INC = ${shell ${PYTHON} -c 'import mpi4py; print(mpi4py.get_include())'}
PYBIND11_INC = ${shell ${PYTHON} -c 'import pybind11; print(pybind11.get_include())'}
NUMPY_INC = ${shell ${PYTHON} -c 'import numpy; print(numpy.get_include())'}

CXXFLAGS = -std=c++11 -fPIC -fopenmp
CXXFLAGS += ${shell ${PYTHON_CONFIG} --includes}
CXXFLAGS += -I$(MPI4PY_INC) -I$(PYBIND11_INC) -I$(NUMPY_INC)

LDFLAGS = -shared
LDFLAGS += ${shell ${PYTHON_CONFIG} --ldflags}
LDFLAGS += -lgomp -lpthread -lm -ldl

build: helloworld.so

helloworld.so: helloworld.cpp
	${MPICXX} -o $@ $< ${CXXFLAGS} ${LDFLAGS}

test: build
	${MPIEXEC} ${NPFLAG} ${PYTHON} test.py

clean:
	rm -f helloworld.so
