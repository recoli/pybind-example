#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

static void sayhello(MPI_Comm comm)
{
  if (comm == MPI_COMM_NULL) {
    std::cout << "You passed MPI_COMM_NULL !!!" << std::endl;
    return;
  }
  int size;
  MPI_Comm_size(comm, &size);
  int rank;
  MPI_Comm_rank(comm, &rank);
  int plen; char pname[MPI_MAX_PROCESSOR_NAME];
  MPI_Get_processor_name(pname, &plen);

  string output = "";
  #pragma omp parallel for
  for (int i = 0; i < omp_get_num_threads(); i++) {
      stringstream ss;
      ss << "OpenMP rank " << omp_get_thread_num() << " of " << omp_get_num_threads() 
         << " from MPI rank " << rank << " of " << size << " on " << pname << std::endl;
      cout << ss.str();
  }
}


#include <pybind11/pybind11.h>
#include <mpi4py/mpi4py.h>
namespace py = pybind11;

static void hw_sayhello(py::object py_comm)
{
  PyObject* py_obj = py_comm.ptr();
  MPI_Comm *comm_p = PyMPIComm_Get(py_obj);
  if (comm_p == NULL) throw py::error_already_set();
  sayhello(*comm_p);
}

PYBIND11_MODULE(helloworld, m)
{
  if (import_mpi4py() < 0) return;

  m.def("sayhello", &hw_sayhello);
}
